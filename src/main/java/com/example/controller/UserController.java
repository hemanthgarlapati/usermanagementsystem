package com.example.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bussiness.UserService;
import com.example.entity.User;
import com.example.exception.AccessDeniedException;
import com.example.exception.UserNotFoundException;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	/**
	 * This is the method for retrieving all the users from the DB.
	 * 
	 * @param args Unused.
	 * @return This method returns response entity with List of users.
	 * @exception UserNotFoundException.
	 */
	@ApiOperation("Retrieve all the users")
	@GetMapping
	public ResponseEntity<List<User>> getAllUsers(@RequestHeader Integer id)
			throws UserNotFoundException, AccessDeniedException {

		List<User> users = userService.getAllUsers(id);

		if (!users.isEmpty()) {
			return ResponseEntity.ok().body(users);
		} else {
			throw new UserNotFoundException("Users not found");
		}
	}

	/**
	 * This is the method for retrieving the user by passing id.
	 * 
	 * @param id.
	 * @return This method returns response entity with user.
	 * @exception UserNotFoundException.
	 */
	@ApiOperation("Retrieves the userInfo based on ID")
	@GetMapping(value = "/{id}")
	public ResponseEntity<User> getEmployeeById(@PathVariable Integer id)
			throws UserNotFoundException, AccessDeniedException {

		return ResponseEntity.ok().body(userService.findById(id));

	}

	/**
	 * This is the method for inserting the user.
	 * 
	 * @param user.
	 * @return This method returns response entity with user.
	 * @exception Nothing.
	 */
	@ApiOperation("Inserts new user into DB")
	@PostMapping
	public ResponseEntity<User> insertUser( @Valid @RequestBody User user)
			throws AccessDeniedException, UserNotFoundException {
		return ResponseEntity.ok().body(userService.addUser(user));

	}

	/**
	 * This is the method for updating the user.
	 * 
	 * @param user.
	 * @param id.
	 * @return This method returns response entity with success message.
	 * @exception Nothing.
	 */
	@ApiOperation("Updates existing user")
	@PutMapping(value = "/{id}")
	public ResponseEntity<String> updateUser( @Valid @RequestBody User user,
			@PathVariable Integer id) throws UserNotFoundException, AccessDeniedException {

		userService.addUser(user);
		return new ResponseEntity<>("Employee with ID: " + user.getId() + " is updated successfully", HttpStatus.OK);

	}

	/**
	 * This is the method for deleting the user.
	 * 
	 * @param id.
	 * @return This method returns response entity with success message.
	 * @exception Nothing.
	 */
	@ApiOperation("Deletes user from DB based on ID")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<String> deleteUser(@RequestHeader Integer uid, @PathVariable Integer id)
			throws UserNotFoundException, AccessDeniedException {
		userService.deleteUser(uid,id);

		return new ResponseEntity<>("Employee with ID: " + id + " is deleted successfully", HttpStatus.OK);

	}

}