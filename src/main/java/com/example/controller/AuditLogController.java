/**
 * 
 */
package com.example.controller;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bussiness.AuditLogService;
import com.example.entity.AuditLog;

import io.swagger.annotations.ApiOperation;

/**
 * @author hemanth.garlapati
 * 
 */
@RestController
@RequestMapping(value="auditLogs")
public class AuditLogController {
	
	@Autowired
	AuditLogService auditLogService;
	
	
	private static final Logger logger = LoggerFactory.getLogger(AuditLogController.class);
	
	/**
	 * This is the method for retrieving  the log details.
	 * @param Nothing.
	 * @return This method returns response entity with list of auditLog.
	 * @exception Nothing.
	 */
	@ApiOperation("Retrieves logs details")
	@GetMapping
	public ResponseEntity<List<AuditLog>> getLogDetails() {
		logger.debug("retrieving logs");
		return ResponseEntity.ok(auditLogService.retrieveLogs());

	}
	
	@ApiOperation("Retrieves logs details by action")
	@GetMapping(value="/{action}")
	public ResponseEntity<List<AuditLog>> getLogDetailsByFilter(@PathVariable String action) {
		logger.debug("retrieving logs by passing action performed");
		return ResponseEntity.ok(auditLogService.retrieveLogsByFilter(action));

	}
	
	@ApiOperation("Retrieves logs details by date") 
	@GetMapping(value="/log/{date}")
	public ResponseEntity<List<AuditLog>> getLogDetailsByDate(@PathVariable String date) {
		logger.debug("inserting logs by passing action date");
		LocalDate localDate = LocalDate.parse(date);
		return ResponseEntity.ok(auditLogService.retrieveLogsByDate(localDate));

	}

}
