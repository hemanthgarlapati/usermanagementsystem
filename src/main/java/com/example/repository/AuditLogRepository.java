/**
 * 
 */
package com.example.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.entity.AuditLog;

/**
 * @author hemanth.garlapati
 * 
 */
@Repository
public interface AuditLogRepository extends JpaRepository<AuditLog, Integer> {
	
	
	@Query(value="SELECT * FROM AUDITLOG A WHERE A.action_performed= ?1",nativeQuery =true  )
	List<AuditLog> findByAction(String action);
	
	@Query(value="SELECT * FROM AUDITLOG A WHERE A.action_date= ?1",nativeQuery =true  )
	List<AuditLog> findByActionDate(LocalDate actionDate);

}
