package com.example.exception;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author hemanth.garlapati
 * 
 */

@ControllerAdvice
public class UserExceptionAdvice extends ResponseEntityExceptionHandler {

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public void handleAllExceptions(Exception exception) {

	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> handleEmployeeNotFoundException(UserNotFoundException exception) {
		List<String> details = new ArrayList<>();
		details.add(exception.getLocalizedMessage());
		CustomErrorResponse errorRepsonse = new CustomErrorResponse("Employee Not Found", details, LocalDateTime.now());
		return new ResponseEntity<Object>(errorRepsonse, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException exception) {
		List<String> details = new ArrayList<>();
		details.add(exception.getLocalizedMessage());
		CustomErrorResponse errorRepsonse = new CustomErrorResponse("Logged in user dont have permission to do this operation", details, LocalDateTime.now());
		return new ResponseEntity<Object>(errorRepsonse, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(exception.getLocalizedMessage());

		CustomErrorResponse error = new CustomErrorResponse("Validation Failed", details, LocalDateTime.now());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

}
