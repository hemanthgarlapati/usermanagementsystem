/**
 * 
 */
package com.example.bussiness;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.AuditLog;
import com.example.repository.AuditLogRepository;

/**
 * @author hemanth.garlapati
 * 
 */

@Service
public class AuditLogService {

	@Autowired
	private AuditLogRepository auditLogRepository;
	

	private static final Logger logger = LoggerFactory.getLogger(AuditLogService.class);

	/**
	 * This is the method for inserting log to the DB.
	 * 
	 * @param auditLog.
	 * @return This method returns AuditLog.
	 * @exception Nothing.
	 */
	public AuditLog save(AuditLog auditLog) {
		logger.debug("Inserting log details");
		return auditLogRepository.save(auditLog);
	}
	
	
	/**
	 * This is the method for retrieving logs.
	 * 
	 * @param auditLog.
	 * @return This method returns AuditLog.
	 * @exception Nothing.
	 */
	public List<AuditLog> retrieveLogs() {
		logger.info("Retrieving log details");
		return auditLogRepository.findAll();
	}
	
	/**
	 * This is the method for retrieving logs by action performed.
	 * 
	 * @param action.
	 * @return This method returns list of AuditLog.
	 * @exception Nothing.
	 */
	public List<AuditLog> retrieveLogsByFilter(String action) {
		
		return auditLogRepository.findByAction(action);
	}
	
	
	/**
	 * This is the method for retrieving logs by action date.
	 * 
	 * @param actionDate.
	 * @return This method returns list of AuditLog.
	 * @exception Nothing.
	 */
	public List<AuditLog> retrieveLogsByDate(LocalDate actionDate) {
		return auditLogRepository.findByActionDate(actionDate);
	}

}
