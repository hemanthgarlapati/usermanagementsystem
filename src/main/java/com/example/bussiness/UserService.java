package com.example.bussiness;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.AuditLog;
import com.example.entity.User;
import com.example.exception.AccessDeniedException;
import com.example.exception.UserNotFoundException;
import com.example.repository.UserRepository;

/**
 * @author hemanth.garlapati
 * 
 */

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	AuditLogService auditLogService;

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	/**
	 * This is the method for retrieving all the employees from the DB.
	 * 
	 * @param args Unused.
	 * @return This method returns List of users.
	 * @exception Nothing.
	 */
	public List<User> getAllUsers(Integer id) throws UserNotFoundException, AccessDeniedException {
		if (this.findById(id).getRole().equalsIgnoreCase("ADMIN")) {
			logger.info("Retrieving all the users Details");

			List<User> userList = userRepository.findAll();
			AuditLog auditLog = new AuditLog();
			auditLog.setActionPerformed("GET");
			auditLog.setActionDate(LocalDate.now());
			auditLogService.save(auditLog);
			if (userList.isEmpty()) {
				throw new UserNotFoundException("No users Found in DB");
			} else {
				return userList;
			}

		} else {
			throw new AccessDeniedException("Access Denied");
		}

	}

	/**
	 * This is the method for deleting the user.
	 * 
	 * @param id.
	 * @return This method does not return anything.
	 * @exception Nothing.
	 */

	public void deleteUser(Integer uid, Integer id) throws UserNotFoundException, AccessDeniedException {

		if (this.findById(uid).getRole().equalsIgnoreCase("ADMIN")) {
			logger.debug("Deleting the user with ID: ", id);
			AuditLog auditLog = new AuditLog();
			auditLog.setActionPerformed("DELETE");
			auditLog.setActionDate(LocalDate.now());
			auditLogService.save(auditLog);
			try {
				userRepository.deleteById(id);
			} catch (Exception exception) {
				throw new UserNotFoundException("User does not exist");
			}
		} else {
			throw new AccessDeniedException("Access Denied");
		}

	}

	/**
	 * This is the method for adding a new user to the DB.
	 * 
	 * @param user.
	 * @return This method returns user.
	 * @exception Nothing.
	 */
	public User addUser( User user) throws AccessDeniedException, UserNotFoundException {
		
			logger.info("Inserting a new  user");
			AuditLog auditLog = new AuditLog();
			auditLog.setActionPerformed("POST");
			auditLog.setActionDate(LocalDate.now());
			auditLogService.save(auditLog);
			return userRepository.save(user);
		} 
	

	/**
	 * This is the method for searching an user with a particular ID.
	 * 
	 * @param id.
	 * @return This method returns user.
	 * @exception UserNotFoundException when there is no user with that particular
	 *                                  ID.
	 */
	public User findById(Integer id) throws UserNotFoundException, AccessDeniedException {
		logger.debug("searching an user with id ", id);
		// Optional<User> user = userRepository.findById(id);

		/*
		 * if ((ObjectUtils.isEmpty(user))) { throw new
		 * UserNotFoundException("No user Found with ID:" + id); }
		 */

		AuditLog auditLog = new AuditLog();
		auditLog.setActionPerformed("GET");
		auditLog.setActionDate(LocalDate.now());
		auditLogService.save(auditLog);
		return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("No user Found with ID:" + id));

	}

}