/**
 * 
 */
package com.example.bussiness;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.entity.AuditLog;
import com.example.repository.AuditLogRepository;

/**
 * @author hemanth.garlapati
 * 
 */

@SpringBootTest
public class TestAuditLogService {
	
	private static final Logger logger = LoggerFactory.getLogger(TestAuditLogService.class);
	
	@InjectMocks
	AuditLogService auditLogService;
	
	@Mock
	AuditLogRepository  auditLogRepository;
	
	@Mock
	AuditLog auditLog;
	
	@BeforeEach
	public void beforeEach() {
		MockitoAnnotations.initMocks(this);
		auditLog = new AuditLog(1, "ADDUSER", LocalDate.now());
		logger.info("Executes before each test case");
	}
	
	@Test
	public void testSaveAuditLog() {
		when(auditLogRepository.save(auditLog)).thenReturn(auditLog);
		assertEquals(auditLog, auditLogService.save(auditLog));
	}
	
	@Test
	public void testGetAllAuditLog() {
		List<AuditLog> auditList=new ArrayList<>();
		auditList.add(auditLog);
		auditList.add(new AuditLog(2, "ADDUSER", LocalDate.now()));
		when(auditLogRepository.findAll()).thenReturn(auditList);
		assertEquals(2, auditLogService.retrieveLogs().size());
	}
	
	@Test
	public void testRetrieveLogsByFilter() {
		List<AuditLog> auditList=new ArrayList<>();
		auditList.add(auditLog);
		auditList.add(new AuditLog(2, "ADDUSER", LocalDate.now()));
		when(auditLogRepository.findByAction("ADDUSER")).thenReturn(auditList);
		assertEquals(2,auditLogService.retrieveLogsByFilter("ADDUSER").size());
	}
	
	@Test
	public void testRetrieveLogsByDate() {
		List<AuditLog> auditList=new ArrayList<>();
		auditList.add(auditLog);
		auditList.add(new AuditLog(2, "ADDUSER", LocalDate.now()));
		when(auditLogRepository.findByActionDate(LocalDate.of(2020, 06, 17))).thenReturn(auditList);
		assertEquals(2,auditLogService.retrieveLogsByDate(LocalDate.of(2020, 06, 17)).size());
	}
	
	

}
