/**
 * 
 */
package com.example.bussiness;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.entity.User;
import com.example.exception.AccessDeniedException;
import com.example.exception.UserNotFoundException;
import com.example.repository.UserRepository;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class TestUserService {

	@InjectMocks
	UserService userService;

	@Mock
	UserRepository userRepository;

	@Mock
	User user;

	private static final Logger logger = LoggerFactory.getLogger(TestUserService.class);

	@BeforeAll
	public static void init() {

	}

	@BeforeEach
	public void beforeEach() throws UserNotFoundException, AccessDeniedException {
		MockitoAnnotations.initMocks(this);
		user = new User(1, "Test1", "ADMIN");
		userService.addUser(user);
		logger.info("Executes before each test case");
	}

	@Test
	public void testAddEmployee() throws UserNotFoundException, AccessDeniedException {
		when(userRepository.save(user)).thenReturn(user);
		assertEquals(user, userService.addUser(user));
	}

	@Test
	public void testGetEmpById() throws Exception {
		when(userRepository.findById(1)).thenReturn(Optional.of(user));
		assertEquals("Test1", userService.findById(1).getName());
	}

	/*
	 * @Test public void testExceptionGetEmpById() throws Exception {
	 * 
	 * when(userRepository.save(user)).thenReturn(user); //
	 * userService.addEmployee(user);
	 * //assertThrows(EmployeeNotFoundException.class, () ->
	 * userRepository.findById(101));
	 * 
	 */

	@Test
	public void testGetAllEmployees() throws AccessDeniedException, UserNotFoundException {
		List<User> userList = new ArrayList<>();
		userList.add(user);
		userList.add(new User(2, "Test2", "USER"));
		when(userRepository.findAll()).thenReturn(userList);
		assertEquals(2, userService.getAllUsers(1));
	}

	@Test
	public void testDeleteEmployee() throws AccessDeniedException, UserNotFoundException {
		User user=new User(2,"Test2","USER");
		when(userService.addUser(user)).thenReturn(user);
		userService.deleteUser(1,2);
		assertEquals(0, userService.getAllUsers(1));
	}

	@AfterEach
	public void afterEach() {
		logger.info("Executes once after each test case");
	}

	@AfterAll
	public static void destroy() {
	}

}
